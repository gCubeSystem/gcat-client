# gCube Catalogue (gCat) Client

gCube Catalogue (gCat) Client is a library designed to interact with REST APIs exposed by the gCat Service.

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

[gCube Catalogue (gCat) Client](https://wiki.gcube-system.org/gcube/GCat_Service)

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/gcat-client/releases).

## Authors

* **Luca Frosini** ([ORCID](https://orcid.org/0000-0003-3183-2291)) - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## How to Cite this Software

Tell people how to cite this software. 
* Cite an associated paper?
* Use a specific BibTeX entry for the software?

    @software{gcat-client,
		author		= {{Luca Frosini}},
		title		= {gCube Catalogue (gCat) Client},
		abstract	= {gCube Catalogue (gCat) Client is a library designed to interact with REST APIs exposed by the gCat Service.},
		url			= {https://doi.org/10.5281/zenodo.7447655},
		keywords	= {Catalogue, D4Science, gCube}
	}

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)


