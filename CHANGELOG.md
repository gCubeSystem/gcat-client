This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gCube Catalogue (gCat) Client

## [v2.4.1]

- Added support for JSON:API on 'licenses' collection [#24601]
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)


## [v2.4.0] [r5.14.0] - 2022-12-07

- Added dependency to be able to compile with JDK 11


## [v2.3.0] [r5.13.1] - 2022-09-19

- Removed service discovery to to old service class
- Using renamed constant from gcat-api


## [v2.2.0] [r5.11.0] - 2022-05-12

- Added support to manage configurations [#22658]
- Migrated to ServiceClass corresponding to Maven groupId

## [v2.1.0] [r5.7.0] - 2022-01-27

- Added query parameter social_post_notification to override default VRE behaviour [#21345]
- Added support for moderation [#21342]
- Added items bulk delete/purge [#21685]
- Added empty trash API [#13322]


## [v2.0.0] [r5.2.0] - 2021-05-04

- Changed service class 


## [v1.2.2] [r5.0.0] - 2021-02-24

- Added count method for Item collection [#20627]
- Added count method for Organization, Group and Profile collection [#20629]


## [v1.2.1] [r4.18.0] - 2019-12-20

- Fixed distro files and pom according to new release procedure


## [v1.2.0] [r4.15.0] - 2019-11-20

- Enforce Content-Type in requests [#16774]


## [v1.1.0] [r4.14.0] - 2019-05-16

- Added the possibility to enforce gCat service URL
- Added the possibility to disable social post [#13335]
- Added the possibility to get a profile as JSON


## [v1.0.0] [r4.13.1] - 2019-02-26

- First Release [#13216]

