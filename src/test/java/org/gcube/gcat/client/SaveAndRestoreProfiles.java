package org.gcube.gcat.client;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.JavaType;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.library.provider.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SaveAndRestoreProfiles {
	
	private static Logger logger = LoggerFactory.getLogger(SaveAndRestoreProfiles.class);
	
	public static final String DIRECTORY_PATH = ".";
	public static final String USERNAME = "luca.frosini";
	public static final String SAVED_TOKEN_FILE = "generated-token.properties";
	public static final String PROFILE_LIST_FILE = "profiles.json";
	public static final String PROFILE_FILE_EXTENSION = ".xml";
	
	public static final String GCAT_URL_STRING = "https://gcat.d4science.org/gcat";
	
	public List<String> getContexts(){
		List<String> contexts = new ArrayList<>();
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/AGINFRAplus");
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/AGINFRAplus4BioCos");
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/AGINFRAplusDev");
		//contexts.add("/d4science.research-infrastructures.eu/FARM/AGINFRAplusShowcase");
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/AgroClimaticModeling_trial");
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/AgroClimaticModelling");
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/DEMETER");
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/DEMETER_trial");
		//contexts.add("/d4science.research-infrastructures.eu/FARM/EMPHASIS");
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/FMJ_Lab");
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/FoodSecurity");
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/FoodborneOutbreak");
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/NitrogenScrumLab");
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/ORIONKnowledgeHub");
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/RAKIP_portal");
		//contexts.add("/d4science.research-infrastructures.eu/D4Research/RAKIP_trial");
		return contexts;
	}
	
	// @Test
	public void restoreProfiles() throws Exception {
		List<String> contexts = getContexts();
		
		String username = USERNAME;
		for(String context : contexts) {
			UserInfo userInfo = new UserInfo(username, new ArrayList<>());
			String userToken = authorizationService().generateUserToken(userInfo, context);
			logger.info("Token for {} for context {} is {}", username, context, userToken);
			SecurityTokenProvider.instance.set(userToken);
			
			logger.debug("---------------------------------------------------------------------");
			logger.debug("Elaborating Context {}", context);
			try {
				restoreProfiles(context, false);
			}catch (Exception e) {
				logger.error("An error occurred while elaborating profiles from context {}", context, e);
			}
			logger.debug("---------------------------------------------------------------------");
			logger.debug("\n\n\n\n");
		}
		
	}
	
	private void restoreProfiles(String context, boolean restore) throws Exception {
		File contextDirectory = new File(DIRECTORY_PATH, context.replaceAll("/", "_"));
		if(!contextDirectory.exists()) {
			throw new Exception(contextDirectory.getAbsolutePath() + " directory does not exists");
		}
		
		ObjectMapper mapper = new ObjectMapper();
		JavaType arrayType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, String.class);
		
		File profileListFile = new File(contextDirectory, PROFILE_LIST_FILE);
		if(!profileListFile.exists()) {
			throw new Exception(profileListFile.getAbsolutePath() + " file does not exists");
		}
		String profilesString = readFile(profileListFile);
		
		List<String> profiles = mapper.readValue(profilesString, arrayType);
		logger.debug("Got Profiles in context {} are {}\n", context, profiles);
		
		Profile profile = new Profile(new URL(GCAT_URL_STRING));
		
		for(String name : profiles) {
			File profileFile = new File(contextDirectory, name+PROFILE_FILE_EXTENSION);
			String xmlProfile = readFile(profileFile);
			logger.debug("Restoring Profile {} for context {}\n", name, context);
			
			try {
				if(restore) {
					profile.create(name, xmlProfile);
				}
			}catch (Exception e) {
				logger.error("Unable to restore Profile {} for context {}", name, context);
			}
		}
		
		
	}

	private String readFile(File profileListFile) throws Exception {
		StringBuffer stringBuffer = new StringBuffer();
		try(BufferedReader br = new BufferedReader(new FileReader(profileListFile))) {
		    for(String line; (line = br.readLine()) != null; ) {
		    	stringBuffer.append(line);
		    }
		} catch (Exception e) {
			throw e;
		}
		return stringBuffer.toString();
	}

	//@Test
	public void removeProfiles() throws Exception {
		
		List<String> contexts = getContexts();
		
		File directory = new File(DIRECTORY_PATH);
		if(!directory.exists()) {
			directory.mkdirs();
		}
		File tokenFiles = new File(directory, SAVED_TOKEN_FILE);
		
		String username = USERNAME;
		for(String context : contexts) {
			
			UserInfo userInfo = new UserInfo(username, new ArrayList<>());
			String userToken = authorizationService().generateUserToken(userInfo, context);
			logger.info("Token for {} for context {} is {}", username, context, userToken);
			SecurityTokenProvider.instance.set(userToken);
			writeOnFile(tokenFiles, context+"="+userToken);
			
			
			logger.debug("---------------------------------------------------------------------");
			logger.debug("Elaborating Context {}", context);
			try {
				getAndSaveProfiles(context, false);
			}catch (Exception e) {
				logger.error("An error occurred while elaborating profiles from context {}", context);
			}
			logger.debug("---------------------------------------------------------------------");
			logger.debug("\n\n\n\n");
			
		}
		
	}
	
	protected void writeOnFile(File file, String content) throws Exception {
		try(FileWriter fw = new FileWriter(file, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw)) {
			out.println(content);
			out.flush();
		} catch(IOException e) {
			throw e;
		}
	}
	
	private void getAndSaveProfiles(String context, boolean delete) throws Exception {
		File contextDirectory = new File(DIRECTORY_PATH, context.replaceAll("/", "_"));
		if(!contextDirectory.exists()) {
			contextDirectory.mkdirs();
		}
		
		ObjectMapper mapper = new ObjectMapper();
		JavaType arrayType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, String.class);
		
		Profile profile = new Profile(new URL(GCAT_URL_STRING));
		String profilesString = profile.list();
		File profileListFile = new File(contextDirectory, PROFILE_LIST_FILE);
		if(profileListFile.exists()) {
			profileListFile.delete();
		}
		writeOnFile(profileListFile, profilesString);
		
		List<String> profiles = mapper.readValue(profilesString, arrayType);
		logger.debug("Got Profiles in context {} are {}\n", context, profiles);
		
		
		for(String name : profiles) {
			try {
				String profileAsxml = profile.read(name);
				File profileFile = new File(contextDirectory, name+PROFILE_FILE_EXTENSION);
				if(profileFile.exists()) {
					profileFile.delete();
				}
				logger.debug("Saving Profile {} for context {}\n", name, context);
				writeOnFile(profileFile, profileAsxml);
				
			}catch (Exception e) {
				logger.error("Error while saving profile {}", name);
			}
			
			if(delete) {
				profile.delete(name);
			}
		}
	}
	
}
