package org.gcube.gcat.client;

import java.util.concurrent.TimeUnit;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class TrashTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(TrashTest.class);

	protected boolean find(ArrayNode ids, String name) {
		boolean found = false;
		for (int i = 0; i < ids.size(); i++) {
			if (name.compareTo(ids.get(i).asText()) == 0) {
				found = true;
				break;
			}
		}
		return found;
	}

	public ItemTest cleanEnv() throws Exception {
		// Cleanign the env
		ContextTest.setContextByName(VRE);
		ItemTest itemTest = new ItemTest();
		try {
			itemTest.deleteItem(true);
		} catch (Exception e) {
			// It is expected. the env was clean
		}
		return itemTest;
	}

	@Test
	public void testList() throws Exception {
		ObjectMapper mapper = new ObjectMapper();

		ItemTest itemTest = cleanEnv();
		
		ContextTest.setContextByName(VRE);
		itemTest.createItem();
		itemTest.deleteItem(false);

		Trash trash = new Trash();
		String jsonArray = trash.list(true);
		ArrayNode ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		boolean found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(found);

		jsonArray = trash.list(false);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(found);
		
		
		itemTest.deleteItem(true);
		jsonArray = trash.list(true);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(!found);
	}
	
	
	@Test
	public void testListAndEmptyTrash() throws Exception {
		ObjectMapper mapper = new ObjectMapper();

		ItemTest itemTest = cleanEnv();
		
		ContextTest.setContextByName(VRE);
		itemTest.createItem();
		itemTest.deleteItem(false);

		Trash trash = new Trash();
		String jsonArray = trash.list(true);
		ArrayNode ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		boolean found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(found);

		jsonArray = trash.list(false);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(found);
		
		
		trash.empty(true);
		Thread.sleep(TimeUnit.SECONDS.toMillis(1));
		jsonArray = trash.list(true);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(!found);
		
		trash.empty(false);
		Thread.sleep(TimeUnit.SECONDS.toMillis(1));
		jsonArray = trash.list(false);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		Assert.assertTrue(ids.size()==0);
	
	}
	

	public static final String NON_CATALOGUE_ADMIN_USER = "lucio.lelii";

	@Test
	public void testListFromAdmin() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		
		ItemTest itemTest = cleanEnv();

		ContextTest.setContextByName(NON_CATALOGUE_ADMIN_USER + "_" + VRE);
		itemTest = new ItemTest();
		String ret = itemTest.createItem();
		logger.debug(ret);
		itemTest.deleteItem(false);

		// I'm admin
		ContextTest.setContextByName(VRE);
		Trash trash = new Trash();
		
		String jsonArray = trash.list(false);
		ArrayNode ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		boolean found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(found);

		
		jsonArray = trash.list(true);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(!found);

		
		ContextTest.setContextByName(NON_CATALOGUE_ADMIN_USER + "_" + VRE);
		itemTest.deleteItem(true);

		// I'm admin
		ContextTest.setContextByName(VRE);
		jsonArray = trash.list(false);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(!found);
		
	}

	@Test
	public void testListFromNonAdmin() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		
		ItemTest itemTest = cleanEnv();

		ContextTest.setContextByName(VRE);
		itemTest.createItem();
		itemTest.deleteItem(false);

		// He is not admin
		ContextTest.setContextByName(NON_CATALOGUE_ADMIN_USER + "_" + VRE);
		Trash trash = new Trash();

		String jsonArray = trash.list(false);
		ArrayNode ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		boolean found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(!found);

		
		jsonArray = trash.list(true);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(!found);

		
		ContextTest.setContextByName(VRE);
		jsonArray = trash.list(false);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(found);
		

		jsonArray = trash.list(true);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(found);

		
		itemTest.deleteItem(true);

		
		jsonArray = trash.list(false);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(!found);

		jsonArray = trash.list(true);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(!found);

		
		ContextTest.setContextByName(NON_CATALOGUE_ADMIN_USER + "_" + VRE);
		jsonArray = trash.list(false);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(!found);
		

		jsonArray = trash.list(true);
		ids = (ArrayNode) mapper.readTree(jsonArray);
		logger.debug("{}", ids);
		found = find(ids, ItemTest.NAME_VALUE);
		Assert.assertTrue(!found);
		
	}

}
