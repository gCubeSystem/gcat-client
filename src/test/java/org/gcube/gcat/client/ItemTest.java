package org.gcube.gcat.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.gcube.com.fasterxml.jackson.databind.JavaType;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.gcat.api.GCatConstants;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ItemTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(ItemTest.class);
	
	private static final String NAME_KEY = "name";
	public static final String NAME_VALUE = "00";
	
	private static final String TITLE_KEY = "title";
	private static final String TITLE_VALUE = "00 Title";
	
	private static final String PRIVATE_KEY = "private";
	private static final boolean PRIVATE_VALUE = false;
	
	private static final String LICENSE_ID_KEY = "license_id";
	
	private static final String TAGS_KEY = "tags";
	private static final String EXTRAS_KEY = "extras";
	
	private static final String KEY_KEY = "key";
	private static final String VALUE_KEY = "value";
	
	private static final String TYPE_KEY_VALUE = "system:type";
	private static final String TYPE_VALUE_VALUE = "EmptyProfile";
	
	private static final String TAG_VALUE = "MyTag";
	
	private static final String ID_KEY = "id";
	
	@Test
	public void testCount() throws MalformedURLException {
		Item item = new Item();
		int count = item.count();
		logger.debug("The items published in the organization corresponding to the VRE {} are {}", SecretManagerProvider.instance.get().getContext(), count);
	}
	
	public String createItem(ObjectMapper mapper, String licenseID) throws Exception {
		
		Map<String,Object> map = new HashMap<>();
		map.put(NAME_KEY, NAME_VALUE);
		map.put(TITLE_KEY, TITLE_VALUE);
		map.put(PRIVATE_KEY, PRIVATE_VALUE);
		map.put(LICENSE_ID_KEY, licenseID);
		
		List<Map<String,Object>> tags = new ArrayList<>();
		Map<String,Object> tag = new HashMap<>();
		tag.put(NAME_KEY, TAG_VALUE);
		tags.add(tag);
		map.put(TAGS_KEY, tags);
		
		List<Map<String,Object>> extras = new ArrayList<>();
		Map<String,Object> type = new HashMap<>();
		type.put(KEY_KEY, TYPE_KEY_VALUE);
		type.put(VALUE_KEY, TYPE_VALUE_VALUE);
		extras.add(type);
		map.put(EXTRAS_KEY, extras);
		
		Item item = new Item();
		String json = mapper.writeValueAsString(map);
		logger.debug("Going to create {}", json);
		return item.create(json);
	}
	
	public String getLicenseID(ObjectMapper mapper) throws Exception {
		JavaType licenseArrayType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, JsonNode.class);
		License license = new License();
		String licencesStrings = license.list();
		logger.debug("Got licenses {}", licencesStrings);
		List<JsonNode> licences = mapper.readValue(licencesStrings, licenseArrayType);
		JsonNode licenseJsonNode = licences.get(0);
		String licenseID = licenseJsonNode.get(ID_KEY).asText();
		return licenseID;
	}
	
	public String createItem() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String licenseID = getLicenseID(mapper);
		return createItem(mapper, licenseID);
	}
	
	public void  deleteItem(boolean purge) throws Exception {
		Item item = new Item();
		item.delete(NAME_VALUE, purge);
	}
	
	
	// @Ignore
	@Test
	public void completeTest() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		JavaType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, String.class);
		
		String licenseID = getLicenseID(mapper);
		
		createItem(mapper, licenseID);
		Item item = new Item();
		try {
			String itemsString = item.list(10, 0);
			logger.debug("Got items {}", itemsString);
			
			List<String> items = mapper.readValue(itemsString, listType);
			String name = items.get(0);
			Assert.assertTrue(name.compareTo(NAME_VALUE) == 0);
		} catch(AssertionError e) {
			item.delete(NAME_VALUE, true);
			throw e;
		}
		
		try {
			String itemString = item.read(NAME_VALUE);
			logger.debug("Got item {}", itemString);
			JsonNode jsonNode = mapper.readTree(itemString);
			
			String gotName = jsonNode.get(NAME_KEY).asText();
			Assert.assertTrue(gotName.compareTo(NAME_VALUE) == 0);
			
			String gotTitle = jsonNode.get(TITLE_KEY).asText();
			Assert.assertTrue(gotTitle.compareTo(TITLE_VALUE) == 0);
			
			boolean privateValue = jsonNode.get(PRIVATE_KEY).asBoolean();
			Assert.assertTrue(privateValue == PRIVATE_VALUE);
			
			String gotLicenseID = jsonNode.get(LICENSE_ID_KEY).asText();
			Assert.assertTrue(gotLicenseID.compareTo(licenseID) == 0);
		} catch(Throwable e) {
			item.delete(NAME_VALUE, true);
			throw e;
		}
		
		item.delete(NAME_VALUE, true);
		
		String itemsString = item.list(10, 0);
		logger.debug("Got list", itemsString);
		List<String> items = mapper.readValue(itemsString, listType);
		if(items.size()>0) {
			String name = items.get(0);
			Assert.assertTrue(name.compareTo(NAME_VALUE) != 0);
		}
	}
	
	// @Ignore
	@Test
	public void listOnVOTest() throws Exception {
		ContextTest.setContextByName(VRE);
		
		Item item = new Item();
		String itemsString = item.list(10, 0, "devvre");
		logger.debug("Got list {}", itemsString);
		
		/*
		Map<String,String> queryParams = new HashMap<>();
		queryParams.put(GCatConstants.Q_KEY, "organization:org1 OR organization:org2");
		itemsString = item.list(queryParams);
		logger.debug("Got list {}", itemsString);
		
		queryParams = new HashMap<>();
		queryParams.put(GCatConstants.Q_KEY, "organization:org3");
		queryParams.put("rows", "4");
		queryParams.put("start", "4");
		itemsString = item.list(queryParams);
		logger.debug("Got list {}", itemsString);
		*/
		
		/*
		ObjectMapper mapper = new ObjectMapper();
		JavaType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, String.class);
		List<String> items = mapper.readValue(itemsString, listType);
		*/
	}
	
	@Test
	public void safeTest() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		JavaType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, String.class);
		
		Item item = new Item();
		String itemsString = item.list(10, 0);
		logger.debug("Got list {}", itemsString);
		
		List<String> items = mapper.readValue(itemsString, listType);
		if(items.size()>0) {
			String name = items.get(0);
			
			String ret = item.read(name);
			logger.debug("Got item {}", ret);
			JsonNode jsonNode = mapper.readTree(ret);
			
			String gotName = jsonNode.get(NAME_KEY).asText();
			Assert.assertTrue(name.compareTo(gotName) == 0);
		}
	}
	
	protected static final String SEARCHABLE_KEY = "searchable";
	public static final String GCAT_URL_STRING = "https://gcat.d4science.org/gcat";
	
	@Ignore
	// @Test
	public void findNotSearchable() throws Exception {
		ContextTest.setContextByName("/d4science.research-infrastructures.eu/SoBigData/ResourceCatalogue");
		ObjectMapper mapper = new ObjectMapper();
		JavaType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, String.class);
		Item item = new Item(new URL(GCAT_URL_STRING));
		String itemsString = item.list(300, 0);
		logger.debug("Got items are {}", itemsString);
		List<String> items = mapper.readValue(itemsString, listType);
		while(items.size() > 0) {
			for(String itemName : items) {
				logger.debug("Analysing item {}", itemName);
				String itemString = item.read(itemName);
				// logger.trace(itemString);
				JsonNode itemNode = mapper.readTree(itemString);
				if(itemNode.has(SEARCHABLE_KEY)){
					logger.warn("{} has {}={}\n{}", itemName, SEARCHABLE_KEY, itemNode.get(SEARCHABLE_KEY).asBoolean(), itemsString);
				}
				Thread.sleep(50);
			}
			Thread.sleep(TimeUnit.SECONDS.toMillis(5));
		}
	}
	
	@Ignore
	// @Test
	public void purgeAll() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		JavaType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, String.class);
		Item item = new Item();
		String itemsString = item.list(200, 0);
		List<String> items = mapper.readValue(itemsString, listType);
		while(items.size() > 0) {
			for(String itemName : items) {
				item.purge(itemName);
				Thread.sleep(50);
			}
			itemsString = item.list(200, 0);
			items = mapper.readValue(itemsString, listType);
			Thread.sleep(TimeUnit.SECONDS.toMillis(5));
		}
	}
	
	// @Ignore
	@Test
	public void testBulkPurge() throws Exception {
		Item item = new Item();
		Map<String,String> queryParams = new HashMap<>();
		queryParams.put(GCatConstants.OWN_ONLY_QUERY_PARAMETER, "false");
		item.bulkDelete(queryParams, false);
	}
	
	
	@Test
	public void testModeration() throws Exception {
		ContextTest.setContextByName("/pred4s/preprod/Dorne");
		Item item = new Item();
		item.reject("my_first_restful_transaction_model_private", "Questo item non mi piace");
	}
	
}
