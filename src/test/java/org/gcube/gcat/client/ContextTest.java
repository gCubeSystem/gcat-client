package org.gcube.gcat.client;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Properties;

import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.GCubeSecret;
import org.gcube.common.authorization.utils.secret.Secret;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(ContextTest.class);
	
	protected static Properties properties;
	protected static final String PROPERTIES_FILENAME = "token.properties";
	
	protected static final String GCAT_PROPERTIES_FILENAME = "gcat.properties"; 
	public static final String GCAT_URL_PROPERTY = "GCAT_URL_PROPERTY";
	public static String GCAT_URL;
	
	public static final String ROOT;
	public static final String VO;
	public static final String VRE;
	
	static {
		properties = new Properties();
		InputStream input = ContextTest.class.getClassLoader().getResourceAsStream(PROPERTIES_FILENAME);
		
		try {
			// load the properties file
			properties.load(input);
		} catch(IOException e) {
			throw new RuntimeException(e);
		}

		ROOT = "/gcube";
		VO = ROOT + "/devsec";
		VRE = VO + "/devVRE";
		
		Properties gcatProperties = new Properties();
		input = ContextTest.class.getClassLoader().getResourceAsStream(GCAT_PROPERTIES_FILENAME);
		try {
			// load the properties file
			gcatProperties.load(input);
			GCAT_URL = gcatProperties.getProperty(GCAT_URL_PROPERTY);
		} catch (Exception e) {
			// throw new RuntimeException(e);
			GCAT_URL = null;
		}
		
		if(GCAT_URL!=null){
			try {
				GCatClientDiscovery.forceToURL(GCAT_URL);
			} catch (MalformedURLException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public static void set(Secret secret) throws Exception {
		SecretManagerProvider.instance.reset();
		SecretManager secretManager = new SecretManager(); 
		SecretManagerProvider.instance.set(secretManager);
		secretManager.addSecret(secret);
		secretManager.set();
		String username = secretManager.getUser().getUsername();
		String context = secretManager.getContext();
		logger.debug("Set authorization for user {} in context {}", username, context);
	}
	
	public static void setContext(String token) throws Exception {
		Secret secret = getSecret(token);
		set(secret);
	}

	public static void setContextByName(String fullContextName) throws Exception {
		Secret secret = getSecretByContextName(fullContextName);
		set(secret);
	}
	
	private static Secret getSecret(String token) throws Exception {
		GCubeSecret secret = new GCubeSecret(token);
		return secret;
	}
	
	private static Secret getSecretByContextName(String fullContextName) throws Exception {
		String token = ContextTest.properties.getProperty(fullContextName);
		return getSecret(token);
	}
	
	@BeforeClass
	public static void beforeClass() throws Exception {
		setContextByName(VRE);
	}
	
	@AfterClass
	public static void afterClass() throws Exception {
		SecretManagerProvider.instance.reset();
	}
	
}
