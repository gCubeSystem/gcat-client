package org.gcube.gcat.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.HttpHeaders;

import org.gcube.com.fasterxml.jackson.databind.JavaType;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.gcat.api.GCatConstants;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class LicenseTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(LicenseTest.class);
	
	private static final String ID_KEY = "id";
	
	@Test
	public void testList() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		JavaType licenseArrayType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, JsonNode.class);
		License license = new License();
		String licencesStrings = license.list();
		logger.debug("Got licenses {}", licencesStrings);
		List<JsonNode> licences = mapper.readValue(licencesStrings, licenseArrayType);
		Assert.assertTrue(licences.size()>0);
		for(JsonNode licenseJsonNode : licences) {
			String licenseID = licenseJsonNode.get(ID_KEY).asText();
			logger.debug("License : {}", licenseID);
		}
	}
	
	@Test
	public void testListAsJSONAPI() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		License license = new License();
		license.addHeader(HttpHeaders.ACCEPT, GCatConstants.APPLICATION_JSON_API);
		String licencesStrings = license.list();
		logger.debug("Got licenses {}", licencesStrings);
		JsonNode licencesWrapper = mapper.readTree(licencesStrings);
		ArrayNode licences = (ArrayNode) licencesWrapper.get(GCatConstants.JSON_API_DATA_FIELD_NAME); 
		Assert.assertTrue(licences.size()>0);
		for(JsonNode licenseJsonNode : licences) {
			String licenseID = licenseJsonNode.get(ID_KEY).asText();
			logger.debug("License : {}", licenseID);
		}
	}
}
