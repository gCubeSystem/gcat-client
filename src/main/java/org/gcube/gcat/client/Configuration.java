package org.gcube.gcat.client;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.ws.WebServiceException;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.gcat.api.configuration.CatalogueConfiguration;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Configuration extends GCatClient implements org.gcube.gcat.api.interfaces.Configuration<CatalogueConfiguration,Void> {
	
	protected ObjectMapper mapper;
	
	public Configuration() throws MalformedURLException {
		super(CONFIGURATIONS);
		mapper = new ObjectMapper();
	}
	
	public Configuration(URL enforcedServiceURL) throws MalformedURLException {
		super(enforcedServiceURL, CONFIGURATIONS);
		mapper = new ObjectMapper();
	}

	protected CatalogueConfiguration getCatalogueConfigurationFromString(String json) throws WebServiceException {
		try {
			return mapper.readValue(json, CatalogueConfiguration.class);
		} catch (WebServiceException e) {
			throw e;
		} catch (Exception e) {
			throw new WebServiceException(e);
		}
	}
	
	@Override
	public CatalogueConfiguration create(String json) throws WebServiceException {
		String ret = super.create(json);
		return getCatalogueConfigurationFromString(ret);
	}

	@Override
	public CatalogueConfiguration read() throws WebServiceException {
		String ret = super.read(CURRENT_CONTEXT_PATH_PARAMETER);
		return getCatalogueConfigurationFromString(ret);
	}

	@Override
	public CatalogueConfiguration update(String json) throws WebServiceException {
		String ret = super.update(json, CURRENT_CONTEXT_PATH_PARAMETER);
		return getCatalogueConfigurationFromString(ret);
	}

	@Override
	public CatalogueConfiguration patch(String json) throws WebServiceException {
		String ret = super.patch(json, CURRENT_CONTEXT_PATH_PARAMETER);
		return getCatalogueConfigurationFromString(ret);
	}

	@Override
	public Void delete() throws WebServiceException {
		super.delete(false, CURRENT_CONTEXT_PATH_PARAMETER);
		return null;
	}

	@Override
	public Void purge() throws WebServiceException {
		super.delete(true, CURRENT_CONTEXT_PATH_PARAMETER);
		return null;
	}

}
