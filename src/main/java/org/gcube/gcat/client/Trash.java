package org.gcube.gcat.client;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.xml.ws.WebServiceException;

import org.gcube.gcat.api.GCatConstants;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class Trash extends GCatClient implements org.gcube.gcat.api.interfaces.Trash<Void> {
	
	public Trash() throws MalformedURLException {
		super(TRASH);
	}
	
	@Override
	public String list(Boolean ownOnly) throws WebServiceException {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put(GCatConstants.OWN_ONLY_QUERY_PARAMETER, String.valueOf(ownOnly));
		String ret = this.list(queryParams);
		return ret;
	}

	@Override
	public Void empty(Boolean ownOnly) throws WebServiceException {
		try {
			initRequest();
			Map<String, String> queryParams = new HashMap<>();
			queryParams.put(GCatConstants.OWN_ONLY_QUERY_PARAMETER, String.valueOf(ownOnly));
			gxhttpStringRequest.queryParams(queryParams);
			HttpURLConnection httpURLConnection = gxhttpStringRequest.delete();
			parseHttpURLConnection(httpURLConnection);
			return null;
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new WebApplicationException(e);
		}
	}

}
