package org.gcube.gcat.client;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.xml.ws.WebServiceException;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.gcat.api.GCatConstants;
import org.gcube.gcat.api.moderation.CMItemStatus;
import org.gcube.gcat.api.moderation.ModerationContent;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Item extends GCatClient implements org.gcube.gcat.api.interfaces.Item<String,Void> {
	
	public Item() throws MalformedURLException {
		super(ITEMS);
	}
	
	public Item(URL enforcedServiceURL) throws MalformedURLException {
		super(enforcedServiceURL, ITEMS);
	}
	
	public int count() throws WebServiceException {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put(GCatConstants.COUNT_QUERY_PARAMETER, String.valueOf(true));
		String ret = this.list(queryParams);
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			JsonNode jsonNode = objectMapper.readTree(ret);
			return jsonNode.get(GCatConstants.COUNT_KEY).asInt();
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new WebApplicationException(e);
		}
	}
	
	/**
	 * List the item in the organisation correspondent to the current VRE.
	 * 
	 * If the client is entitled to run at VO or ROOT level the method return all the item in all the organization
	 * in the catalogue. To filter per organisation used the method {@link #list(int, int, String)}
	 */
	@Override
	public String list(int limit, int offset) throws WebApplicationException {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put(GCatConstants.LIMIT_QUERY_PARAMETER, String.valueOf(limit));
		queryParams.put(GCatConstants.OFFSET_QUERY_PARAMETER, String.valueOf(offset));
		return this.list(queryParams);
	}
	
	public String list(Map<String,String> queryParams) throws WebApplicationException {
		return super.list(queryParams);
	}
	
	/**
	 * List the item of a specific organisation. 
	 * This API is only available if the client is entitles to run at VO and ROOT level.
	 */
	public String list(int limit, int offset, String organizationName) throws WebApplicationException {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put(GCatConstants.LIMIT_QUERY_PARAMETER, String.valueOf(limit));
		queryParams.put(GCatConstants.OFFSET_QUERY_PARAMETER, String.valueOf(offset));
		queryParams.put(GCatConstants.Q_KEY, String.format(GCatConstants.ORGANIZATION_FILTER_TEMPLATE, organizationName));
		return super.list(queryParams);
	}
	
	public String create(String json, boolean socialPost) throws WebApplicationException {
		try {
			Map<String,String> queryParams = new HashMap<>();
			queryParams.put(GCatConstants.SOCIAL_POST_QUERY_PARAMETER, String.valueOf(socialPost));
			return super.create(json, queryParams);
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new WebApplicationException(e);
		}
	}
	
	@Override
	public String create(String json) throws WebApplicationException {
		return super.create(json);
	}

	@Override
	public String read(String name) throws WebApplicationException {
		return super.read(name);
	}

	@Override
	public String update(String name, String json) throws WebApplicationException {
		return super.update(json, name);
	}

	@Override
	public String patch(String name, String json) throws WebApplicationException {
		return super.patch(json, name);
	}
	
	@Override
	public Void delete(String name) throws WebApplicationException {
		super.delete(false, name);
		return null;
	}

	@Override
	public Void delete(String name, boolean purge) throws WebServiceException {
		super.delete(purge, name);
		return null;
	}

	@Override
	public Void purge(String name) throws WebServiceException {
		super.delete(true, name);
		return null;
	}
	
	
	public Void bulkDelete(Map<String,String> queryParams, boolean purge) throws WebServiceException{
		try {
			initRequest();
			if(queryParams==null) {
				queryParams = new HashMap<>();
			}
			queryParams.put(GCatConstants.PURGE_QUERY_PARAMETER, String.valueOf(purge));
			gxhttpStringRequest.queryParams(queryParams);
			HttpURLConnection httpURLConnection = gxhttpStringRequest.delete();
			parseHttpURLConnection(httpURLConnection);
			return null;
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new WebApplicationException(e);
		}
	}
	
	public Void bulkDelete(boolean purge) throws WebServiceException{
		return bulkDelete(null, purge);
	}
	
	public Void bulkPurge() throws WebServiceException {
		return bulkDelete(null, true);
	}

	protected String moderate(String name, ModerationContent moderationContent) {
		return moderate(name, moderationContent, null);
	}
	
	protected String moderate(String name, ModerationContent moderationContent, Map<String,String> queryParams) {
		try {
			initRequest();
			gxhttpStringRequest.path(name);
			if(queryParams!=null && queryParams.size()>0) {
				gxhttpStringRequest.queryParams(queryParams);
			}
			String moderationContentString = (new ObjectMapper()).writeValueAsString(moderationContent);
			gxhttpStringRequest.header("Content-Type", GCatConstants.APPLICATION_JSON_CHARSET_UTF_8);
			gxhttpStringRequest.withBody(moderationContentString);
			HttpURLConnection httpURLConnection = gxhttpStringRequest.post();
			return parseHttpURLConnection(httpURLConnection);
		}catch (WebApplicationException e) {
			throw e;
		}catch (Exception e) {
			throw new WebApplicationException(e);
		}
	}
	
	public String approve(String name, String moderatorMessage) {
		ModerationContent moderationContent = new ModerationContent();
		moderationContent.setCMItemStatus(CMItemStatus.APPROVED);
		moderationContent.setMessage(moderatorMessage);
		return approve(name, moderatorMessage, false);
	}
	
	public String approve(String name, String moderatorMessage, boolean socialPost) {
		ModerationContent moderationContent = new ModerationContent();
		moderationContent.setCMItemStatus(CMItemStatus.APPROVED);
		moderationContent.setMessage(moderatorMessage);
		Map<String,String> queryParams = new HashMap<>();
		queryParams.put(GCatConstants.SOCIAL_POST_QUERY_PARAMETER, String.valueOf(socialPost));
		return moderate(name, moderationContent, queryParams);
	}

	public String reject(String name, String moderatorMessage) {
		ModerationContent moderationContent = new ModerationContent();
		moderationContent.setCMItemStatus(CMItemStatus.REJECTED);
		moderationContent.setMessage(moderatorMessage);
		return moderate(name, moderationContent);
	}

	public void message(String name, String message) {
		ModerationContent moderationContent = new ModerationContent();
		moderationContent.setMessage(message);
		moderate(name, moderationContent);	
	}

	
}
